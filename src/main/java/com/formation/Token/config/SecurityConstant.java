package com.formation.Token.config;

public class SecurityConstant {
	  public static final String AUTH_LOGIN_URL = "/login";
	  public static final String JWT_SECRET = "nZr4u7x!A%D*F-JaNdRgUkXp2s5v8y/B?E(H+KbPeShVmYq3t6w9z$C&F)J@NcQf"; //clé cryptée 512 bite
	  public static final String TOKEN_HEADER = "Authorization";
	  public static final String TOKEN_PREFIX = "Bearer";
	  public static final String TOKEN_TYPE = "JWT";
	  public static final String TOKEN_ISSUER = "secure-api";
	  public static final String TOKEN_AUDIENCE = "secure-app";
	  
	  private void SecurityConstants() 
	  {
		  throw new IllegalStateException("Cannot create instance of static util class");
	  }
}